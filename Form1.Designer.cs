﻿namespace calculadora_nomina
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSueldo = new System.Windows.Forms.Label();
            this.lblAgui = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSueldoMensual = new System.Windows.Forms.TextBox();
            this.txtAgui = new System.Windows.Forms.TextBox();
            this.txtVaca = new System.Windows.Forms.TextBox();
            this.txtInfonavit = new System.Windows.Forms.TextBox();
            this.txtIMSS = new System.Windows.Forms.TextBox();
            this.txtRCV = new System.Windows.Forms.TextBox();
            this.lblVaca = new System.Windows.Forms.Label();
            this.lblInfonavit = new System.Windows.Forms.Label();
            this.lblIMSS = new System.Windows.Forms.Label();
            this.lblRCV = new System.Windows.Forms.Label();
            this.txtValesDespensa = new System.Windows.Forms.TextBox();
            this.txtComedor = new System.Windows.Forms.TextBox();
            this.lblValeDesp = new System.Windows.Forms.Label();
            this.lblComedor = new System.Windows.Forms.Label();
            this.lblSeguroDeVida = new System.Windows.Forms.Label();
            this.txtSeguroDeVida = new System.Windows.Forms.TextBox();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.lblSalarioDiario = new System.Windows.Forms.Label();
            this.txtSueldoDiario = new System.Windows.Forms.TextBox();
            this.lblDiasNomina = new System.Windows.Forms.Label();
            this.txtDiasNomina = new System.Windows.Forms.TextBox();
            this.txtDiasVacaciones = new System.Windows.Forms.TextBox();
            this.lblDiasVacaciones = new System.Windows.Forms.Label();
            this.lblSueldoNeto = new System.Windows.Forms.Label();
            this.txtSueldoNeto = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblSueldo
            // 
            this.lblSueldo.AutoSize = true;
            this.lblSueldo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSueldo.ForeColor = System.Drawing.Color.Black;
            this.lblSueldo.Location = new System.Drawing.Point(33, 9);
            this.lblSueldo.Name = "lblSueldo";
            this.lblSueldo.Size = new System.Drawing.Size(220, 16);
            this.lblSueldo.TabIndex = 0;
            this.lblSueldo.Text = "INGRESE SUELDO MENSUAL:";
            // 
            // lblAgui
            // 
            this.lblAgui.AutoSize = true;
            this.lblAgui.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgui.ForeColor = System.Drawing.Color.Black;
            this.lblAgui.Location = new System.Drawing.Point(406, 47);
            this.lblAgui.Name = "lblAgui";
            this.lblAgui.Size = new System.Drawing.Size(82, 16);
            this.lblAgui.TabIndex = 1;
            this.lblAgui.Text = "Aguinaldo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 2;
            // 
            // txtSueldoMensual
            // 
            this.txtSueldoMensual.Location = new System.Drawing.Point(259, 5);
            this.txtSueldoMensual.Multiline = true;
            this.txtSueldoMensual.Name = "txtSueldoMensual";
            this.txtSueldoMensual.Size = new System.Drawing.Size(125, 20);
            this.txtSueldoMensual.TabIndex = 3;
            // 
            // txtAgui
            // 
            this.txtAgui.Location = new System.Drawing.Point(512, 44);
            this.txtAgui.Name = "txtAgui";
            this.txtAgui.Size = new System.Drawing.Size(80, 20);
            this.txtAgui.TabIndex = 4;
            // 
            // txtVaca
            // 
            this.txtVaca.Location = new System.Drawing.Point(512, 75);
            this.txtVaca.Name = "txtVaca";
            this.txtVaca.Size = new System.Drawing.Size(80, 20);
            this.txtVaca.TabIndex = 5;
            // 
            // txtInfonavit
            // 
            this.txtInfonavit.Location = new System.Drawing.Point(512, 105);
            this.txtInfonavit.Name = "txtInfonavit";
            this.txtInfonavit.Size = new System.Drawing.Size(80, 20);
            this.txtInfonavit.TabIndex = 6;
            // 
            // txtIMSS
            // 
            this.txtIMSS.Location = new System.Drawing.Point(512, 133);
            this.txtIMSS.Name = "txtIMSS";
            this.txtIMSS.Size = new System.Drawing.Size(80, 20);
            this.txtIMSS.TabIndex = 7;
            // 
            // txtRCV
            // 
            this.txtRCV.Location = new System.Drawing.Point(512, 167);
            this.txtRCV.Name = "txtRCV";
            this.txtRCV.Size = new System.Drawing.Size(80, 20);
            this.txtRCV.TabIndex = 8;
            // 
            // lblVaca
            // 
            this.lblVaca.AutoSize = true;
            this.lblVaca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVaca.Location = new System.Drawing.Point(406, 79);
            this.lblVaca.Name = "lblVaca";
            this.lblVaca.Size = new System.Drawing.Size(94, 16);
            this.lblVaca.TabIndex = 9;
            this.lblVaca.Text = "Vacaciones:\r\n";
            // 
            // lblInfonavit
            // 
            this.lblInfonavit.AutoSize = true;
            this.lblInfonavit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfonavit.Location = new System.Drawing.Point(406, 109);
            this.lblInfonavit.Name = "lblInfonavit";
            this.lblInfonavit.Size = new System.Drawing.Size(70, 16);
            this.lblInfonavit.TabIndex = 10;
            this.lblInfonavit.Text = "Infonavit:";
            // 
            // lblIMSS
            // 
            this.lblIMSS.AutoSize = true;
            this.lblIMSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIMSS.Location = new System.Drawing.Point(406, 137);
            this.lblIMSS.Name = "lblIMSS";
            this.lblIMSS.Size = new System.Drawing.Size(48, 16);
            this.lblIMSS.TabIndex = 11;
            this.lblIMSS.Text = "IMSS:";
            // 
            // lblRCV
            // 
            this.lblRCV.AutoSize = true;
            this.lblRCV.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRCV.Location = new System.Drawing.Point(406, 171);
            this.lblRCV.Name = "lblRCV";
            this.lblRCV.Size = new System.Drawing.Size(43, 16);
            this.lblRCV.TabIndex = 12;
            this.lblRCV.Text = "RCV:";
            // 
            // txtValesDespensa
            // 
            this.txtValesDespensa.Location = new System.Drawing.Point(284, 132);
            this.txtValesDespensa.Name = "txtValesDespensa";
            this.txtValesDespensa.Size = new System.Drawing.Size(100, 20);
            this.txtValesDespensa.TabIndex = 15;
            // 
            // txtComedor
            // 
            this.txtComedor.Location = new System.Drawing.Point(284, 158);
            this.txtComedor.Name = "txtComedor";
            this.txtComedor.Size = new System.Drawing.Size(100, 20);
            this.txtComedor.TabIndex = 16;
            // 
            // lblValeDesp
            // 
            this.lblValeDesp.AutoSize = true;
            this.lblValeDesp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValeDesp.Location = new System.Drawing.Point(131, 133);
            this.lblValeDesp.Name = "lblValeDesp";
            this.lblValeDesp.Size = new System.Drawing.Size(147, 16);
            this.lblValeDesp.TabIndex = 17;
            this.lblValeDesp.Text = "Vales de despensa:";
            // 
            // lblComedor
            // 
            this.lblComedor.AutoSize = true;
            this.lblComedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComedor.Location = new System.Drawing.Point(134, 162);
            this.lblComedor.Name = "lblComedor";
            this.lblComedor.Size = new System.Drawing.Size(75, 16);
            this.lblComedor.TabIndex = 18;
            this.lblComedor.Text = "Comedor:";
            // 
            // lblSeguroDeVida
            // 
            this.lblSeguroDeVida.AutoSize = true;
            this.lblSeguroDeVida.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeguroDeVida.Location = new System.Drawing.Point(131, 188);
            this.lblSeguroDeVida.Name = "lblSeguroDeVida";
            this.lblSeguroDeVida.Size = new System.Drawing.Size(120, 16);
            this.lblSeguroDeVida.TabIndex = 19;
            this.lblSeguroDeVida.Text = "Seguro de Vida:";
            // 
            // txtSeguroDeVida
            // 
            this.txtSeguroDeVida.Location = new System.Drawing.Point(284, 184);
            this.txtSeguroDeVida.Name = "txtSeguroDeVida";
            this.txtSeguroDeVida.Size = new System.Drawing.Size(100, 20);
            this.txtSeguroDeVida.TabIndex = 20;
            // 
            // btnCalcular
            // 
            this.btnCalcular.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnCalcular.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalcular.ForeColor = System.Drawing.Color.Black;
            this.btnCalcular.Location = new System.Drawing.Point(36, 279);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 21;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = false;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.Location = new System.Drawing.Point(134, 279);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpiar.TabIndex = 22;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblSalarioDiario
            // 
            this.lblSalarioDiario.AutoSize = true;
            this.lblSalarioDiario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSalarioDiario.Location = new System.Drawing.Point(33, 34);
            this.lblSalarioDiario.Name = "lblSalarioDiario";
            this.lblSalarioDiario.Size = new System.Drawing.Size(131, 16);
            this.lblSalarioDiario.TabIndex = 23;
            this.lblSalarioDiario.Text = "SALARIO DIARIO:";
            // 
            // txtSueldoDiario
            // 
            this.txtSueldoDiario.Location = new System.Drawing.Point(259, 31);
            this.txtSueldoDiario.Name = "txtSueldoDiario";
            this.txtSueldoDiario.Size = new System.Drawing.Size(125, 20);
            this.txtSueldoDiario.TabIndex = 24;
            // 
            // lblDiasNomina
            // 
            this.lblDiasNomina.AutoSize = true;
            this.lblDiasNomina.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiasNomina.Location = new System.Drawing.Point(33, 59);
            this.lblDiasNomina.Name = "lblDiasNomina";
            this.lblDiasNomina.Size = new System.Drawing.Size(110, 16);
            this.lblDiasNomina.TabIndex = 27;
            this.lblDiasNomina.Text = "DIAS NOMINA:";
            // 
            // txtDiasNomina
            // 
            this.txtDiasNomina.Location = new System.Drawing.Point(259, 57);
            this.txtDiasNomina.Name = "txtDiasNomina";
            this.txtDiasNomina.Size = new System.Drawing.Size(125, 20);
            this.txtDiasNomina.TabIndex = 28;
            // 
            // txtDiasVacaciones
            // 
            this.txtDiasVacaciones.Location = new System.Drawing.Point(259, 83);
            this.txtDiasVacaciones.Name = "txtDiasVacaciones";
            this.txtDiasVacaciones.Size = new System.Drawing.Size(125, 20);
            this.txtDiasVacaciones.TabIndex = 29;
            // 
            // lblDiasVacaciones
            // 
            this.lblDiasVacaciones.AutoSize = true;
            this.lblDiasVacaciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiasVacaciones.Location = new System.Drawing.Point(33, 83);
            this.lblDiasVacaciones.Name = "lblDiasVacaciones";
            this.lblDiasVacaciones.Size = new System.Drawing.Size(147, 16);
            this.lblDiasVacaciones.TabIndex = 30;
            this.lblDiasVacaciones.Text = "DIAS VACACIONES:";
            // 
            // lblSueldoNeto
            // 
            this.lblSueldoNeto.AutoSize = true;
            this.lblSueldoNeto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSueldoNeto.Location = new System.Drawing.Point(243, 279);
            this.lblSueldoNeto.Name = "lblSueldoNeto";
            this.lblSueldoNeto.Size = new System.Drawing.Size(119, 16);
            this.lblSueldoNeto.TabIndex = 31;
            this.lblSueldoNeto.Text = "SUELDO NETO:";
            // 
            // txtSueldoNeto
            // 
            this.txtSueldoNeto.Location = new System.Drawing.Point(363, 275);
            this.txtSueldoNeto.Name = "txtSueldoNeto";
            this.txtSueldoNeto.Size = new System.Drawing.Size(100, 20);
            this.txtSueldoNeto.TabIndex = 32;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Orange;
            this.ClientSize = new System.Drawing.Size(625, 314);
            this.Controls.Add(this.txtSueldoNeto);
            this.Controls.Add(this.lblSueldoNeto);
            this.Controls.Add(this.lblDiasVacaciones);
            this.Controls.Add(this.txtDiasVacaciones);
            this.Controls.Add(this.txtDiasNomina);
            this.Controls.Add(this.lblDiasNomina);
            this.Controls.Add(this.txtSueldoDiario);
            this.Controls.Add(this.lblSalarioDiario);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.txtSeguroDeVida);
            this.Controls.Add(this.lblSeguroDeVida);
            this.Controls.Add(this.lblComedor);
            this.Controls.Add(this.lblValeDesp);
            this.Controls.Add(this.txtComedor);
            this.Controls.Add(this.txtValesDespensa);
            this.Controls.Add(this.lblRCV);
            this.Controls.Add(this.lblIMSS);
            this.Controls.Add(this.lblInfonavit);
            this.Controls.Add(this.lblVaca);
            this.Controls.Add(this.txtRCV);
            this.Controls.Add(this.txtIMSS);
            this.Controls.Add(this.txtInfonavit);
            this.Controls.Add(this.txtVaca);
            this.Controls.Add(this.txtAgui);
            this.Controls.Add(this.txtSueldoMensual);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblAgui);
            this.Controls.Add(this.lblSueldo);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "Form1";
            this.Text = "Calculadora de Nomina";
            this.TransparencyKey = System.Drawing.Color.Black;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSueldo;
        private System.Windows.Forms.Label lblAgui;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSueldoMensual;
        private System.Windows.Forms.TextBox txtAgui;
        private System.Windows.Forms.TextBox txtVaca;
        private System.Windows.Forms.TextBox txtInfonavit;
        private System.Windows.Forms.TextBox txtIMSS;
        private System.Windows.Forms.TextBox txtRCV;
        private System.Windows.Forms.Label lblVaca;
        private System.Windows.Forms.Label lblInfonavit;
        private System.Windows.Forms.Label lblIMSS;
        private System.Windows.Forms.Label lblRCV;
        private System.Windows.Forms.TextBox txtValesDespensa;
        private System.Windows.Forms.TextBox txtComedor;
        private System.Windows.Forms.Label lblValeDesp;
        private System.Windows.Forms.Label lblComedor;
        private System.Windows.Forms.Label lblSeguroDeVida;
        private System.Windows.Forms.TextBox txtSeguroDeVida;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Label lblSalarioDiario;
        private System.Windows.Forms.TextBox txtSueldoDiario;
        private System.Windows.Forms.Label lblDiasNomina;
        private System.Windows.Forms.TextBox txtDiasNomina;
        private System.Windows.Forms.TextBox txtDiasVacaciones;
        private System.Windows.Forms.Label lblDiasVacaciones;
        private System.Windows.Forms.Label lblSueldoNeto;
        private System.Windows.Forms.TextBox txtSueldoNeto;
    }
}

