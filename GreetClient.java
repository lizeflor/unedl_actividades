package sockets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


public class GreetClient {
     private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private ServerSocket serverSocket;
    
    public void startConnection(String ip, int port) {
         try {
             clientSocket = new Socket(ip, port);
             out = new PrintWriter(clientSocket.getOutputStream(), true);
             in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
         } catch (IOException ex) {
             Logger.getLogger(GreetClient.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
 
    public String sendMessage(String msg) {
         try {
             out.println(msg);
             String resp = in.readLine();
             return resp;
         } catch (IOException ex) {
             Logger.getLogger(GreetClient.class.getName()).log(Level.SEVERE, null, ex);
         }
         return null;
    }
 
    public void stopConnection() {
         try {
             in.close();
             out.close();
             clientSocket.close();
         } catch (IOException ex) {
             Logger.getLogger(GreetClient.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
     public void stop() {
        try {
            in.close();
            out.close();
            clientSocket.close();
            serverSocket.close();
        } catch (IOException ex) {
           Logger.getLogger(GreetClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
  }