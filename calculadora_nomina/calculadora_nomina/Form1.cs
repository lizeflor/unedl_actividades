﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calculadora_nomina
{
    public partial class Form1 : Form
    {
        Double SueldoMensual, SueldoDiario, DiasNomina, DiasVacaciones;
        Double Aguinaldo, Vacaciones, Infonavit, IMSS, RCV, ValesDespensa, SueldoNeto;
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtSueldoMensual.Clear();
            txtAgui.Clear();
            txtComedor.Clear();
            txtIMSS.Clear();
            txtRCV.Clear();
            txtInfonavit.Clear();
            txtSeguroDeVida.Clear();
            txtValesDespensa.Clear();
            txtVaca.Clear();
            txtSueldoDiario.Clear();
            txtDiasNomina.Clear();
            txtDiasVacaciones.Clear();
            txtSueldoNeto.Clear();
            
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            SueldoMensual = Double.Parse(txtSueldoMensual.Text);
            DiasNomina = Double.Parse(txtDiasNomina.Text);
            DiasVacaciones = Double.Parse(txtDiasVacaciones.Text);


            Aguinaldo = SueldoMensual / DiasNomina * 15;
            txtAgui.Text ="$"+ Aguinaldo.ToString();

            SueldoDiario = SueldoMensual / DiasNomina;
            txtSueldoDiario.Text = "$" + SueldoDiario.ToString();

            Vacaciones = SueldoDiario * DiasVacaciones * .25;
            txtVaca.Text ="$" + Vacaciones.ToString();


            IMSS = SueldoMensual / DiasNomina * 0.0025 + SueldoMensual / DiasNomina * 0.00375 + SueldoMensual / DiasNomina * 0.00625;
            txtIMSS.Text = "$" + IMSS.ToString();
            

            Infonavit = SueldoMensual * 0.05;
            txtInfonavit.Text = "$" + Infonavit.ToString();

            RCV = (SueldoMensual) * (0.01125) * 2;
            txtRCV.Text = "$" + RCV.ToString();

            ValesDespensa = SueldoMensual * 0.01;
            txtValesDespensa.Text = "$" + ValesDespensa.ToString();

            SueldoNeto = SueldoMensual - RCV - ValesDespensa - Infonavit - IMSS;
            txtSueldoNeto.Text = "$" + SueldoNeto.ToString();



        }
    }
}
