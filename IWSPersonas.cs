﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace WSEjemplo
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IWSPersonas
    {
        [OperationContract]
        Persona ObtenerPersona(string Identificacion);

        // TODO: agregue aquí sus operaciones de servicio
    }

    [DataContract]
    public class Persona: BaseRespuesta
    {//DataMember: forma parte de un contrato de datos  y se puede serializar
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public int Edad { get; set; }
       [DataMember]
        public string Direccion { get; set; }
        [DataMember]
        public string Telefono { get; set; }
        [DataMember]
        public string Sexo{ get; set; }
        public string Secreto { get; set; }
       
    }

    [DataContract]
    public class BaseRespuesta
    {
        [DataMember]
        public string MensajeRespuesta { get; set; }
        [DataMember]
        public string Error { get; set; }
    }
}
