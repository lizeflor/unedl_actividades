﻿using System;
using System.Windows.Forms;

namespace ClienteWS
{
    public partial class FormPruebaWCF : Form
    {
        public FormPruebaWCF()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var identificacion = txtIdentificacion.Text;
            using (WSPersonas.WSPersonasClient client = new WSPersonas.WSPersonasClient())
            {
                var persona = client.ObtenerPersona(identificacion);
                var nombre = persona.Nombre;
                var edad = persona.Edad;
                var direccion = persona.Direccion;
                var telefono = persona.Telefono;
                var sexo = persona.Sexo;

                txtNombre.Text = nombre.ToString();
                txtEdad.Text = edad.ToString();
                txtDir.Text = edad.ToString();
                txtTel.Text = telefono.ToString();
                txtSexo.Text = sexo.ToString();
            }

        }
    }
}
