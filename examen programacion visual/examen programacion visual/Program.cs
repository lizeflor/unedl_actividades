﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace examen_programacion_visual
{
    class Program
    {
        private static Semaphore pooldechalanes;
        private static int a = 0;
        private static int b = 0;
        private static int c = 0;
        public static void ConstruyePared(object o)
        {
            int i = 1000;
            while (i-- > 0)
            {
                Console.Write(o);
            }
        }

        public static void habitacion(object o)
        {
            Console.WriteLine("INICIANDO EL CUARTO: {0}", o);
            pooldechalanes.WaitOne();
            var paredes = new Task[4];
            for (int i = 0; i <= paredes.Length - 1; i++)
            {
                paredes[i] = Task.Run(() => ConstruyePared('.'));
            }
            try
            {
                Task.WaitAll(paredes);
                Console.WriteLine("------------------");
            }
            catch (AggregateException ae)
            {
                Console.WriteLine("One or more exceptions occurred: ");
                foreach (var ex in ae.Flatten().InnerExceptions)
                    Console.WriteLine("   {0}", ex.Message);
            }
            foreach (var p in paredes)
                Console.WriteLine(" pared: {0}: {1}", p.Id, p.Status);
            Console.WriteLine("cuarto {0} terminado, turno {1}", o,
                pooldechalanes.Release());
        }

        public static void Main(string[] args)
        {
            

            Console.WriteLine("¿CUANTOS CUARTOS?");
            a = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿CUANTOS TRABAJADORES PUEDEN ENTRAR?");
            b = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("¿CUANTOS SEGUNDOS HAY PARA CONSTRUIR?");
            c = Convert.ToInt32(Console.ReadLine()) * 1000;

            pooldechalanes = new Semaphore(0, b);

            Console.WriteLine("INICIA LA CONSTRUCCION");

            for (int i = 1; i <= a; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(habitacion));
                t.Start(i);
            }

            Thread.Sleep(500);
            Console.WriteLine("SE LIBERA UN TURNO");
            pooldechalanes.Release(b);

            Thread.Sleep(c);

            Console.WriteLine("CONSTRUCCION TERMINADA");
            Console.ReadKey();
        }
    }
}


