﻿namespace EXAMEN_TALLER_DE_PROGRAMACION
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCASA_A_FABRICAR = new System.Windows.Forms.TextBox();
            this.txtHABITACIONEES_POR_CASA = new System.Windows.Forms.TextBox();
            this.txtBAÑOS = new System.Windows.Forms.TextBox();
           // this.btnIniciar_c = new System.Windows.Forms.Button();
            this.lblPersonas_disponibles = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(193, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ingrese la cantidad de casas a fabricar:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(225, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ingrese  la cantidad de habitaciones por casa:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(197, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = " Ingrese  la cantidad de baños por casa:";
            // 
            // txtCASA_A_FABRICAR
            // 
            this.txtCASA_A_FABRICAR.Location = new System.Drawing.Point(245, 17);
            this.txtCASA_A_FABRICAR.Name = "txtCASA_A_FABRICAR";
            this.txtCASA_A_FABRICAR.Size = new System.Drawing.Size(100, 20);
            this.txtCASA_A_FABRICAR.TabIndex = 3;
            // 
            // txtHABITACIONEES_POR_CASA
            // 
            this.txtHABITACIONEES_POR_CASA.Location = new System.Drawing.Point(243, 55);
            this.txtHABITACIONEES_POR_CASA.Name = "txtHABITACIONEES_POR_CASA";
            this.txtHABITACIONEES_POR_CASA.Size = new System.Drawing.Size(100, 20);
            this.txtHABITACIONEES_POR_CASA.TabIndex = 4;
            // 
            // txtBAÑOS
            // 
            this.txtBAÑOS.Location = new System.Drawing.Point(243, 100);
            this.txtBAÑOS.Name = "txtBAÑOS";
            this.txtBAÑOS.Size = new System.Drawing.Size(100, 20);
            this.txtBAÑOS.TabIndex = 5;
            // 
            // btnIniciar_construccion
            // 
            this.btnIniciar_construccion.Location = new System.Drawing.Point(507, 19);
            this.btnIniciar_construccion.Name = "btnIniciar_construccion";
            this.btnIniciar_construccion.Size = new System.Drawing.Size(168, 23);
            this.btnIniciar_construccion.TabIndex = 6;
            this.btnIniciar_construccion.Text = "INICIAR CONSTRUCCION";
            this.btnIniciar_construccion.UseVisualStyleBackColor = true;
           // this.btnIniciar_construccion.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // lblPersonas_disponibles
            // 
            this.lblPersonas_disponibles.AutoSize = true;
            this.lblPersonas_disponibles.Location = new System.Drawing.Point(6, 27);
            this.lblPersonas_disponibles.Name = "lblPersonas_disponibles";
            this.lblPersonas_disponibles.Size = new System.Drawing.Size(288, 13);
            this.lblPersonas_disponibles.TabIndex = 7;
            this.lblPersonas_disponibles.Text = "CANTIDAD DE PERSONAS DISPONIBLES A TRABAJAR:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(214, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "CANTIDAD DE PERSONAS A INGRESAR:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblPersonas_disponibles);
            this.groupBox1.Location = new System.Drawing.Point(31, 180);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(354, 146);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SEMAFORO";
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(842, 412);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnIniciar_construccion);
            this.Controls.Add(this.txtBAÑOS);
            this.Controls.Add(this.txtHABITACIONEES_POR_CASA);
            this.Controls.Add(this.txtCASA_A_FABRICAR);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnIniciar;
        private System.Windows.Forms.Label lblCasas_a_fabricar;
        private System.Windows.Forms.Label lblHabitaciones_por_casa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCasas_a_fabricar;
        private System.Windows.Forms.TextBox txtBaños_por_casa;
        private System.Windows.Forms.TextBox txtHabitaciones_por_casa;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCASA_A_FABRICAR;
        private System.Windows.Forms.TextBox txtHABITACIONEES_POR_CASA;
        private System.Windows.Forms.TextBox txtBAÑOS;
        private System.Windows.Forms.Button btnIniciar_construccion;
        private System.Windows.Forms.Label lblPersonas_disponibles;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

