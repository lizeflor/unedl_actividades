﻿using System;
using System.Windows.Forms;

namespace Simulador_Carrera_de_Carros
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new CarreraCarros());
        }
    }
}
