﻿using System;


namespace DelegadoSencillo
{
    class CRadio
    {
        public static void MetodoRadio(string pMensaje)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;//poner color primer plano de la consola, es decir de los caracteres mostrados
            Console.WriteLine("Estamos en la clase Radio");
            Console.WriteLine("Este es tu mensaje {0}",pMensaje);
        }
    }
}
