﻿using System;

namespace DelegadoSencillo
{
    
     //un delegado debe tener el mismo tipo de dato de retorno, misma cantidad de parametros y estos deben ser del mismo tipo y mismo orden
     

    //definicion del delegado
    public delegate void MiDelegado(string m);
    class DelegadoSencillo
    {
        static void Main(string[] args)
        {
            //creacion y referencia a un metodo
            MiDelegado delegado = new MiDelegado(CRadio.MetodoRadio);
            //por medio del delegado usamos el metodo
            delegado("Hola a todos");

            //apuntar a otro metodo
            delegado = new MiDelegado(CPastel.MostrarPastel);
            //invocar el otro metodo
            delegado("Feliz cumpleaños");

            Console.ReadKey();//evitar que se cierre la consola
        }
    }
}
