﻿using System;


namespace DelegadoSencillo
{
    class CPastel
    {
        public static void MostrarPastel(string pAnuncio)
        {
            Console.ForegroundColor = ConsoleColor.Red;//poner color primer plano de la consola, es decir de los caracteres mostrados
            Console.WriteLine("El mensaje lleva el pastel de {0}",pAnuncio);
        }
    }
}
