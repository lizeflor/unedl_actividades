﻿using System;

/*
 Puede recuperar los datos almacenados en la ubicación a la que hace referencia la variable de puntero,
utilizando el método ToString ().
 */


namespace UnsafeMode3
{
    class UnsafeMode3
    {
        static  unsafe void Main(string[] args)
        {

            int var = 20;
            int* p = &var;
            Console.WriteLine("Data is: {0} ", var);//imprimir valor de la variable
            Console.WriteLine("Data is: {0} ", p->ToString());//imprimir informacion de la variable a la que apunta el puntero
            Console.WriteLine("Address is: {0} ", (int)p);//imprimir direccion de memoria donde esta  la variable p
            Console.ReadKey();
        }
    }
}
