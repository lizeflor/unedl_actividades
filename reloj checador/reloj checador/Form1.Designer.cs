﻿namespace reloj_checador
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1Empleado = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.labelEmpleado = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.groupBoxOperacion = new System.Windows.Forms.GroupBox();
            this.radioButtonSalida = new System.Windows.Forms.RadioButton();
            this.radioButtonEntrada = new System.Windows.Forms.RadioButton();
            this.buttonRegistrar = new System.Windows.Forms.Button();
            this.buttonRevisar = new System.Windows.Forms.Button();
            this.groupBoxOperacion.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox1Empleado
            // 
            this.comboBox1Empleado.FormattingEnabled = true;
            this.comboBox1Empleado.Items.AddRange(new object[] {
            " LIZETH MARGAR",
            " JUAN JESUS",
            " JULIO",
            "AGUILA JULIO",
            "ZARATE SILVIA"});
            this.comboBox1Empleado.Location = new System.Drawing.Point(122, 39);
            this.comboBox1Empleado.Name = "comboBox1Empleado";
            this.comboBox1Empleado.Size = new System.Drawing.Size(268, 21);
            this.comboBox1Empleado.TabIndex = 0;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(122, 83);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(268, 20);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // labelEmpleado
            // 
            this.labelEmpleado.AutoSize = true;
            this.labelEmpleado.Location = new System.Drawing.Point(59, 42);
            this.labelEmpleado.Name = "labelEmpleado";
            this.labelEmpleado.Size = new System.Drawing.Size(57, 13);
            this.labelEmpleado.TabIndex = 2;
            this.labelEmpleado.Text = "Empleado:";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Location = new System.Drawing.Point(48, 89);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(68, 13);
            this.lblFecha.TabIndex = 3;
            this.lblFecha.Text = "Fecha/Hora:";
            // 
            // groupBoxOperacion
            // 
            this.groupBoxOperacion.Controls.Add(this.radioButtonSalida);
            this.groupBoxOperacion.Controls.Add(this.radioButtonEntrada);
            this.groupBoxOperacion.Location = new System.Drawing.Point(122, 128);
            this.groupBoxOperacion.Name = "groupBoxOperacion";
            this.groupBoxOperacion.Size = new System.Drawing.Size(268, 54);
            this.groupBoxOperacion.TabIndex = 4;
            this.groupBoxOperacion.TabStop = false;
            this.groupBoxOperacion.Text = "Operacion";
            // 
            // radioButtonSalida
            // 
            this.radioButtonSalida.AutoSize = true;
            this.radioButtonSalida.Location = new System.Drawing.Point(171, 31);
            this.radioButtonSalida.Name = "radioButtonSalida";
            this.radioButtonSalida.Size = new System.Drawing.Size(54, 17);
            this.radioButtonSalida.TabIndex = 1;
            this.radioButtonSalida.TabStop = true;
            this.radioButtonSalida.Text = "Salida";
            this.radioButtonSalida.UseVisualStyleBackColor = true;
            this.radioButtonSalida.CheckedChanged += new System.EventHandler(this.radioButtonSalida_CheckedChanged);
            // 
            // radioButtonEntrada
            // 
            this.radioButtonEntrada.AutoSize = true;
            this.radioButtonEntrada.Location = new System.Drawing.Point(6, 31);
            this.radioButtonEntrada.Name = "radioButtonEntrada";
            this.radioButtonEntrada.Size = new System.Drawing.Size(62, 17);
            this.radioButtonEntrada.TabIndex = 0;
            this.radioButtonEntrada.TabStop = true;
            this.radioButtonEntrada.Text = "Entrada";
            this.radioButtonEntrada.UseVisualStyleBackColor = true;
            this.radioButtonEntrada.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // buttonRegistrar
            // 
            this.buttonRegistrar.Location = new System.Drawing.Point(115, 238);
            this.buttonRegistrar.Name = "buttonRegistrar";
            this.buttonRegistrar.Size = new System.Drawing.Size(75, 23);
            this.buttonRegistrar.TabIndex = 5;
            this.buttonRegistrar.Text = "Registrar";
            this.buttonRegistrar.UseVisualStyleBackColor = true;
            this.buttonRegistrar.Click += new System.EventHandler(this.buttonRegistrar_Click);
            // 
            // buttonRevisar
            // 
            this.buttonRevisar.Location = new System.Drawing.Point(272, 238);
            this.buttonRevisar.Name = "buttonRevisar";
            this.buttonRevisar.Size = new System.Drawing.Size(75, 23);
            this.buttonRevisar.TabIndex = 6;
            this.buttonRevisar.Text = "Revisar";
            this.buttonRevisar.UseVisualStyleBackColor = true;
            this.buttonRevisar.Click += new System.EventHandler(this.buttonRevisar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 307);
            this.Controls.Add(this.buttonRevisar);
            this.Controls.Add(this.buttonRegistrar);
            this.Controls.Add(this.groupBoxOperacion);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.labelEmpleado);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.comboBox1Empleado);
            this.Name = "Form1";
            this.Text = "ConboBoxEmpleado";
            this.groupBoxOperacion.ResumeLayout(false);
            this.groupBoxOperacion.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1Empleado;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label labelEmpleado;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.GroupBox groupBoxOperacion;
        private System.Windows.Forms.RadioButton radioButtonEntrada;
        private System.Windows.Forms.RadioButton radioButtonSalida;
        private System.Windows.Forms.Button buttonRegistrar;
        private System.Windows.Forms.Button buttonRevisar;
    }
}

